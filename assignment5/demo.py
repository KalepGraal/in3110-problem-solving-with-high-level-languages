# Some sample python code, that does not run at all,
# just used for as a syntax highlighting example


def load_theme(theme_path):
    with open(theme_path) as file:
        for line in file.readlines():
            print("hello" + str(True))          # prints hello


# python comment
if 2 == 'b':
    print("okay")
elif 2 == '2':
    assert 2 == 2
else:
    while True:
        print("oh no")

numbers = [22, 34, 12, 32, 4]


class Demo:
    def __init__(self, real, imaginary):
        """ Constructor Complex class

        :param real: numeric value representing the real part of the
        complex number
        :param imaginary: numeric value representing the imaginary part of
        the complex number
        :returns new Complex object
        """
        self.real = real
        self.imaginary = imaginary
