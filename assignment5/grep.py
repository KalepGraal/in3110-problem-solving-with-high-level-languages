import re
import argparse
from highlighter import Highlighter

if __name__ == '__main__':
    """ Finds lines in file matching given regex patterns """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
                 "--highlight",
                 action="store_true",
                 help="supplied if search result should be highlighted")
    arg_parser.add_argument("input_file", type=str,
                            help="filename of file to search in")
    arg_parser.add_argument(
              "regex", type=str,
              help="regex to use when searching in file, supports multiple", nargs="*")
    args = arg_parser.parse_args()

    # Create theme and syntax dictionaries used when highlighting
    theme = {0: "0;31", 1: "0;32", 2: "0;33", 3: "0;34", 4: "0;35"}
    syntax = {}
    for i, regex in enumerate(args.regex):
        syntax[regex] = i % len(theme)

    output = ""
    if args.highlight:

        # create highlighter and apply theme
        hl = Highlighter(theme, syntax)
        output = hl.apply_theme(args.input_file)
    else:
        # not highlighting, read the file ourselves
        with open(args.input_file) as f:
            output = f.read()

    # Print lines matching regexes
    for line in output.splitlines():
        should_print = False
        for regex in syntax.keys():
            if re.search(regex, line):
                should_print = True
        if should_print:
            print(line)
