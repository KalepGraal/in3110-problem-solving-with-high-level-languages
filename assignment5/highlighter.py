import sys
import re

reset_color = "\033[0m"


class Highlighter:
    """ Class used to highlight text with colors. """
    theme = {}
    syntax = {}
    stack = [reset_color]

    def __init__(self, theme, syntax):
        """ Creates a highlighter instance.

        :arg theme: dictionary containing theme to use when highlighting
        :arg syntax: dictionary containing regexes used to define highlight
        syntax. """
        self.theme = theme
        self.syntax = syntax

    def color(self, code):
        """ Transforms color code to fully formatted color string from theme.

        :arg code: color code to use
        :return formatted color string"""
        return f"\033[{self.theme[code]}m"

    def fix_coloring(self, match):
        """ Lazy fix for overlapping coloring.

        Works by keeping track of colors currently used in a stack, and
        replacing sections where colors are reset, but that should instead
        be set to previously used color.

        :arg match: a regex Match, from which to extract color string
        """
        color = match.group(0)
        if color == reset_color:
            if len(self.stack) == 1:
                # no more colors, setting to default
                return reset_color
            else:
                # remove color no longer used, set next color to
                # previously used
                self.stack.pop()
                return self.stack[-1]
        else:
            self.stack.append(color)
            return color

    def fix_overlap(self, string):
        """ Finds all color codes, and fixes any overlapping colors (if any).

        :arg string: string containing text with possible coloring overlaps """
        # This regex finds color markers such as "\033[0;31m"
        return re.sub(r"\033\[0;?\d*m", self.fix_coloring, string)

    def apply_theme(self, src):
        """ Uses regexes in provided syntax file to insert colors according
            to provided theme.

        :arg src: (string) source file to get text to color from
        :return (string) colored text"""

        with open(src) as file:
            string = file.read()
            for regex, style in self.syntax.items():
                string = re.sub(f"({regex})", self.color(style) + r'\1'
                                + reset_color, string)
            #  handle cases where supplied regexes would overlap each other
            string = self.fix_overlap(string)

        return string


def load_theme(theme_path):
    """ Loads theme settings from file

    :arg theme_path: path to file containing theme describing colors to use."""
    theme = {}
    with open(theme_path) as file:
        for line in file.readlines():
            formatted = line.split(": ")
            key = formatted[0]
            val = formatted[1].rstrip()  # removes \n

            theme[key] = val
    return theme


def load_syntax(syntax_path):
    """ Loads language syntax in regex form from file.

    :arg syntax_path: path to file containing regexes describing syntax
    to use"""
    syntax = {}
    with open(syntax_path) as file:
        for line in file.readlines():
            formatted = line.split(": ")
            key = formatted[0][1:-1]  # removes quotes
            val = formatted[1].rstrip()  # removes \n

            syntax[key] = val
    return syntax


if __name__ == '__main__':
    """ highlights text in a file, from given syntax and color-theme.
    Prints results, and outputs to formatted.txt """
    syntax_file = sys.argv[1]
    theme_file = sys.argv[2]
    source_file = sys.argv[3]

    theme = load_theme(theme_file)
    syntax = load_syntax(syntax_file)

    colored_text = Highlighter(theme, syntax).apply_theme(source_file)

    with open("formatted.txt", "w+") as file_to_write:
        file_to_write.write(colored_text)

    print(colored_text)
