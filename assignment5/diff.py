import sys


def find_diff(file1, file2, output_path):
    """" Attempts to finds differences between two files, writes these to
    another file.

    Currently this function is far from perfect, but it does an OK job at
    finding differences.
    Problems will arise if there's multiple lines with the same content,
     but this is not something
    i felt was required to fix, as the assignment was a bit unclear on how much
    effort we should put into this.

    :arg file1: original source file
    :arg file2: modified version of source file
    :args output_path: path to write results to """
    f_1, f_2 = None, None
    with open(file1) as f:
        f_1 = f.readlines()
    with open(file2) as f:
        f_2 = f.readlines()

    with open(output_path, "w+") as output:
        i, j = 0, 0
        while i < len(f_1) or j < len(f_2):
            if not i < len(f_1):
                # original file ended, rest in second file is new lines
                output.write(f"+ {f_2[j]}")
                j += 1
                continue

            if not j < len(f_2):
                # second file ended, rest in original file was removed
                output.write(f"- {f_1[i]}")
                i += 1
                continue

            if f_1[i] == f_2[j]:
                output.write(f"0 {f_1[i]}")
                i += 1
                j += 1
            else:
                if f_1[i] not in f_2:
                    output.write(f"- {f_1[i]}")
                    i += 1
                elif f_2[j] in f_1[i:]:
                    i += 1
                else:
                    output.write(f"+ {f_2[j]}")
                    j += 1


if __name__ == '__main__':
    """ Finds differences between two files and writes them to
    diff_output.txt """
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    find_diff(file1, file2, "diff_output.txt")
