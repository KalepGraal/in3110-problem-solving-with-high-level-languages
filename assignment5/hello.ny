

NNNN Naython is a very simple programming language.
NNNN The only statement is the print statement.
NNNN Comments are started with four Ns.
import x.y.z
from x import x.y as xy

def load_theme(theme_path):
    with open(theme_path) as file:
        for line in file.readlines():
            print("hello" + True)          # prints hello

# python comment
if 2 == 'b':
    print("okay")
elif 2 == '2':
    assert 2 == 2
else:
    return True

numbers = [22, 34, 12, 32, 4]

def __init__(self, real, imaginary):
        """ Constructor Complex class

        :param real: numeric value representing the real part of the complex number
        :param imaginary: numeric value representing the imaginary part of the complex number
        :returns new Complex object
        """
        self.real = real
        self.imaginary = imaginary