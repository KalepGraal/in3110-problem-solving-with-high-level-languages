# Assignment 5 - Regular expressions

## 5.1 Syntax highlighting
Syntax highlighting is handled by highlighter.py.  
It assumes that (quotes from assignment):
 - "You can assume that comments in code never contain actual code."  

Run with `python3 highlighter.py <syntax-file> <theme-file> <file-to-highlight>`  
Output is printed to the terminal and also stored in `formatted.txt`.

## 5.2 Python syntax
Python syntax can be found in `python.syntax`.  
Test command: `python3 highlighter.py python.syntax python.theme demo.py`

the items i've chosen for the language colorings (both python and java) are:
 - Comments
 - Function definitions
 - Class definitions
 - Strings
 - Imports
 - "Special" statements None, True, False
 - Variable assignments
 - Try/except
 - for-loops
 - while-loops
 - if/elif/else blocks

### some regex documentation
``"#.*(?:$|\n)"`` ensures any characters after a # is matched until a newline or the end of the string/file.  
``"(?<=def )(\w*)"`` matches any word after a `def` occurrence.  
``"(?<!def )(?<= |\.)(\w*)(?=\()"`` matches any word not prefixed by `def ` but with a whitespace or a dot, and that ends with an opening paranthesis. (function calls).  
Otherwise i think most regexes are just variations of these, or really basic.

## 5.3 Syntax for Java
Here i've created syntax for Java, which can be found in `favorite_language.syntax`.  
Test command: `python3 highlighter.py favorite_language.syntax favorite_language.theme test.java`
 
## 5.4 grep
`grep.py` supports an arbitrary number of arguments, and can also be used with the `--highlight` flag.  

More info can be found by running. `python3 grep.py --help`

## 5.5 superdiff
`diff.py` finds the difference between two files, and outputs it to `diff_output.txt`. This function is far from 
perfect, but it should cover basic cases (more info in docstring).  

usage example: `python3 diff.py test_diff-a.txt test_diff-b.txt`

## 5.6 Coloring diff
Created `diff.syntax` and `diff.theme` so that the output of diff can be used with the highlighter.

usage example: `python3 highlighter.py diff.syntax diff.theme diff_output.txt`