// Sample java-code taken from IN1010

class Lenkeliste<T> implements Liste<T>{
    """ Implementasjon av en lenkeliste """
    Node start = null;
    private int antElementer = 0;

    public int stoerrelse(){
        return antElementer;
    }

    public void leggTil(int pos, T x){
        if (pos < 0 || pos > antElementer) throw new UgyldigListeIndeks(pos);
        else if (antElementer == 0) start = new Node(x);
        else if (pos == 0) {
            Node temp = start;
            start = new Node(x);
            start.neste = temp;
        } else if (hentNodePaaIndeks(pos) == null) {
            hentNodePaaIndeks(pos - 1).neste = new Node(x);
        }
        else {
            Node n = hentNodePaaIndeks(pos - 1);
            Node temp = n.neste;
            n.neste = new Node(x);
            n.neste.neste = temp;
        }
        antElementer++;
    }

    public void leggTil(T x){
        if (antElementer == 0) start = new Node(x);
        else {
            Node n = start;
            while (n.neste != null) {
                n = n.neste;
            }
            n.neste = new Node(x);
        }
        antElementer++;
    }

    public void sett(int pos, T x){
        Node n = hentNodePaaIndeks(pos);
        if (n == null || pos < 0){
            throw new UgyldigListeIndeks(pos);
        }
        hentNodePaaIndeks(pos).data = x;
    }

    public T hent(int pos){
        if (pos < 0) throw new UgyldigListeIndeks(-1);
        Node n = hentNodePaaIndeks(pos);
        if (n == null) throw new UgyldigListeIndeks(pos);
        return hentNodePaaIndeks(pos).data;
    }

    public T fjern(int pos){
        if (pos == 0) return fjern();
        else if (hentNodePaaIndeks(pos) == null || pos < 0){
            throw new UgyldigListeIndeks(pos);
        }
        Node forrige = hentNodePaaIndeks(pos - 1);
        Node fjernes = forrige.neste;
        forrige.neste = fjernes.neste;
        antElementer--;
        return fjernes.data;
    }

    public T fjern(){
        if (start == null){
            throw new UgyldigListeIndeks(-1);
        }
        Node fjernes = start;
        start = fjernes.neste;
        antElementer--;
        return fjernes.data;
    }

    private Node hentNodePaaIndeks(int indeks){
        Node n = start;
        for (int i = 0; i < indeks; i++){
            n = n.neste;
        }
        return n;
    }

    class Node {
        Node neste = null;
        T data;

        Node(T t){data = t;}
    }
}