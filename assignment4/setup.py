from distutils.core import setup

setup(
    name='image blurrer',
    version='1.0',
    packages=['image_blurrer', 'image_blurrer.implementations']
)