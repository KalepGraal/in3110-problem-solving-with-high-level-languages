import cv2
import numpy
import time
from numba import jit


def blur_image(src):
    """ Blurs image using numba optimization

    :arg src source image to blur
    :return blurred image
    """
    new_img = numpy.copy(src)

    # Pad source borders, to make blurring edges possible
    src = numpy.pad(src, ((1, 1), (1, 1), (0, 0)), "edge")

    src = src.astype("uint16")

    return blur(src, new_img)


@jit
def blur(src, dst):
    """ Numba optimized blur function

    Here blur is separated into its own function, to enable JIT compiling without Numba warnings

    :arg src: Padded image array containing image.
    :arg dst: Image array without padding, used to store blurred image
    :return: blurred image (without padding)
    """
    # blur image by averaging values from a pixels surrounding pixels
    for h in range(1, src.shape[0] - 1):
        for w in range(1, src.shape[1] - 1):
            for c in range(0, src.shape[2]):
                dst[h - 1, w - 1, c] = (src[h, w, c] + src[h - 1, w, c] + src[h + 1, w, c]
                                            + src[h, w - 1, c] + src[h, w + 1, c]
                                            + src[h - 1, w - 1, c] + src[h - 1, w + 1, c]
                                            + src[h + 1, w - 1, c] + src[h + 1, w + 1, c]) / 9
    return dst


if __name__ == '__main__':
    """ blurs an image and prints the time used """
    t1 = time.time()

    image = cv2.imread("beatles.jpg")
    print(f"image dimensions: {image.shape}")
    blurred_image = blur_image(image)
    cv2.imwrite("output.jpg", blurred_image)

    t2 = time.time()
    print("ran in: {}s".format(t2-t1))