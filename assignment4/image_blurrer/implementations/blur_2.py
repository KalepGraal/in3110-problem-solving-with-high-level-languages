import cv2
import numpy
import time


def blur_image(src):
    """ Blurs image using vectorized operations

    :arg src source image to blur
    :return blurred image
    """
    new_img = numpy.asarray(numpy.copy(src).astype("uint16"))

    # Pad source borders, to make blurring edges possible
    src = numpy.pad(src, ((1, 1), (1, 1), (0, 0)), "edge")
    src = src.astype("uint16")

    # blur image by averaging values from a pixels surrounding pixels
    new_img[:, :, :] = (src[1:-1, 1:-1, :] + src[:-2, 1:-1, :] + src[2:, 1:-1, :]
                        + src[1:-1, :-2, :] + src[1:-1, 2:, :]
                        + src[:-2, :-2, :] + src[:-2, 2:, :]
                        + src[2:, :-2, :] + src[2:, 2:, :]) / 9

    return new_img


if __name__ == '__main__':
    """ blurs an image and prints the time used """
    t1 = time.time()

    image = cv2.imread("beatles.jpg")
    print(f"image dimensions: {image.shape}")
    blurred_image = blur_image(image)
    cv2.imwrite("output.jpg", blurred_image)

    t2 = time.time()
    print("ran in: {}s".format(t2-t1))
