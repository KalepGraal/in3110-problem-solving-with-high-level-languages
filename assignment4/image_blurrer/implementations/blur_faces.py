import cv2
import time
import pkg_resources

from image_blurrer.implementations.blur_2 import blur_image


def blur_faces(image, loop=False, show_borders=False):
    """ Blurs faces in an image using vectorized operations

    :arg image source image to blur faces in
    :arg loop whether or not to loop until no faces are detected
    :arg show_borders whether or not to draw green outline around blurred faces
    :return image with blurred faces
    """
    # This ensures no problems occur using "pip . install"
    xml = pkg_resources.normalize_path("image_blurrer/implementations/haarcascade_frontalface_default.xml")
    face_cascade = cv2.CascadeClassifier(xml)

    should_run = True
    should_store_faces = True

    stored_faces = None

    while should_run:
        faces = face_cascade.detectMultiScale(
            image,
            scaleFactor=1.025,
            minNeighbors=5,
            minSize=(30, 30)
        )
        if should_store_faces:
            stored_faces = faces
            should_store_faces = False

        for (x, y, w, h) in faces:
            # Blur face using vectorized blurring from blur_2.py
            if loop:
                # To speed-up the blurring, we blur a smaller face and then resize that one
                # (done to make loop-blurring not take a long time)
                before = cv2.resize(image[y:y + h, x:x + w], (0, 0), fx=0.3, fy=0.3)
                after = cv2.resize(blur_image(before), (w, h))
                image[y:y + h, x:x + w] = after
            else:
                # if we're not looping, we'd rather have an even blur
                image[y:y + h, x:x + w] = blur_image(image[y:y + h, x:x + w])

        if not (loop and len(faces) > 0):
            should_run = False

    if show_borders:
        for (x, y, w, h) in stored_faces:
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 1)

    return image


if __name__ == '__main__':
    """ blurs faces in an image and prints the time used """
    t1 = time.time()

    img = cv2.imread("beatles.jpg")

    print(f"image dimensions: {img.shape}")

    blurred_faces = blur_faces(img, True, True)

    cv2.imshow("asdf", blurred_faces)
    cv2.waitKey()

    cv2.imwrite("output.jpg", blurred_faces)
    t2 = time.time()
    print("ran in: {}s".format(t2-t1))
