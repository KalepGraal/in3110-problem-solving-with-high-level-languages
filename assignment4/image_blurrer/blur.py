import argparse
import cv2
from image_blurrer.implementations.blur_1 import blur_image as blur_python
from image_blurrer.implementations.blur_2 import blur_image as blur_vectorized
from image_blurrer.implementations.blur_3 import blur_image as blur_numba
from image_blurrer.implementations.blur_faces import blur_faces

def __arg_parser():
    """ function to parse system arguments

    :return: Namespace containing args parsed
    """
    parser = argparse.ArgumentParser(description="Blurs an image")
    parser.add_argument("input_file", metavar="input-file", type=str,
                        help="filename of image to blur")
    parser.add_argument("output_file", metavar="output-file", type=str, nargs='?',
                        help="filename of blurred image output, defaults to input-file")
    parser.add_argument("-r", action='store_true',
                        help="only used in compile-type='faces', will keep blurring until no faces are found.")
    parser.add_argument("-d", action='store_true',
                        help="only used in compile-type='faces', will draw green outline around blurred faces.")
    parser.add_argument("-c", metavar="compile-type", type=str, default="vectorized",
                        help="type of implementation to use. 'vectorized' (default), 'python', 'numba' or 'faces')",
                        choices=["vectorized", "python", "numba", "faces"])

    return parser.parse_args()


def __blur_image(implementation, input_filename, output_filename=None,
                 recursive=False, outline=False, should_write=True):
    """ Blurs an image and potentially writes to file

    :arg implementation: implementation used when blurring, one of: 'vectorized', 'python', 'numba', 'faces'
    :arg input_filename: filename of image to blur
    :arg output_filename: (optional) filename of output image, will default to input_filename
    :arg recursive: (optional) repeat blurring until no faces are detected
    :arg show borders whether or not to draw green outline around blurred faces
    :arg should_write: (optional) whether image should be written to file
    :return: numpy array containing the blurred images data
    """
    image = cv2.imread(input_filename)

    # blur image using implementation specified by args
    blurred = None
    if implementation == "vectorized":
        blurred = blur_vectorized(image)
    elif implementation == "python":
        blurred = blur_python(image)
    elif implementation == "numba":
        blurred = blur_numba(image)
    elif implementation == "faces":
        blurred = blur_faces(image, recursive, outline)
    else:
        print("Error occurred!")
        exit(-1)

    # output to input file if output is not specified
    output_file = output_filename if output_filename else input_filename

    if should_write:
        cv2.imwrite(output_file, blurred)

    return blurred


def blur_image(input_filename, output_filename=None):
    """ Blurs image from file	
    :arg input_filename: filename of image to blur
    :arg output_filename: filename of file to write to, will not write if not specified
    :return: numpy array containing the blurred images data	
    """
    return __blur_image("vectorized", input_filename, output_filename, output_filename is not None)


if __name__ == '__main__':
    """ blurs an image and writes it to a file """

    args = __arg_parser()
    __blur_image(args.c, args.input_file, args.output_file, recursive=args.r, outline=args.d)
