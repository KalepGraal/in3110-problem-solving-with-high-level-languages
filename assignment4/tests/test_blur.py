import numpy
import cv2
import image_blurrer.implementations.blur_2 as b


def test_max_val_not_increased():
    """ tests that blurring never increases the arrays max value

    This is changed from how the assigment wants it done,
    since if we only check if max is decreased, it will fail when all pixels are equal."""
    array = __create_rand_image()
    max_1 = array.max()
    array = b.blur_image(array)
    max_2 = array.max()
    assert max_1 >= max_2


def test_pixel_is_avg_of_neighbours():
    """ Tests that a pixel after blurring is equal to average of neighbouring pixels before blurring """
    clear = __create_rand_image()
    blurred = b.blur_image(clear)

    assert int(blurred[5][5][2]) == int((clear[5, 5, 2] + clear[4, 5, 2] + clear[6, 5, 2]
                                         + clear[5, 4, 2] + clear[5, 6, 2]
                                         + clear[4, 4, 2] + clear[4, 6, 2]
                                         + clear[6, 4, 2] + clear[6, 6, 2]) / 9)


def __create_rand_image():
    """ Creates a random image (from seed)

    :return numpy array representing an image
    """
    numpy.random.seed(42)
    array = numpy.random.rand(10, 10, 3)
    array = array * 255
    array = array.astype("uint16")

    cv2.imwrite("test.jpg", array)
    return array
