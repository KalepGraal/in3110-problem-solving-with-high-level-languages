# Assignment 4

### 4.1-4.3 Blur implementations
The Python implementation can be found in `blur_1.py`.  
The Numpy implementation can be found in `blur_2.py`.  
The Numba implementation can be found in `blur_3.py`.

Reports on how fast each implementation was can be found in `report2.txt` and `report3.txt`

When running the test programs in `blur_1.py`, `blur_2.py` or  `blur_3.py` the working directory should be set to assignment4, 
to ensure that the program finds the file `beatles.jpg`.

### 4.5 User interface
User interface can be accessed by running blur.py.  
example usage:  
`python3 blur.py in-image.jpg out-image.jpg`


More info can be found by running:  
`python3 blur.py --help`

### 4.6 Packaging and unit tests
#### Packaging
Packages can be installed with (when in dir assignment4):  
`pip3 install .`  
They can then be imported into a python project using either:  
`from image_blurrer.blur import blur_image`    
`blur_image("input.jpg", "output.jpg")`  
or:  
`import image_blurrer.blur as b`  
`b.blur_image("inout.jpg", "output.jpg")`

dependencies can be found in requirements.txt

#### Tests
Tests use pytest, and can be ran with: `python -m pytest` (when in correct dir).

### 4.7 Blurring faces
Using compile-type 'faces', faces can be blurred.  
example: `python3 blur.py beatles.jpg output.jpg -drc faces`

Optional arguments for blurring faces:  
`-d`: Draws green outline around blurred faces.  
`-r`: Repeats blurring until no faces are recognized as faces.