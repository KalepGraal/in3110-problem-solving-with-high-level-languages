#!/bin/bash

# behavior
#   start [label]
#       should start new timer, unless one is already running (error)
#   stop
#       should stop running timer. (error if no task is running?)
#   status 
#       displays what task is currently being tracked.
#   log
#       lists all tasks with time spent from logfile

# LOGFILE should be set in user environment, if not, use default
if [ -z "${LOGFILE}" ]; then 
    LOGFILE="logfile.log"
    echo "using default logfile: logfile.log" 
fi

# check if logfile exists, if not, create it
test -f $LOGFILE || touch $LOGFILE | echo "did not find logfile, created a new one" 

# find first word in last logged line
lastlogged=`tail -n 1 $LOGFILE | cut -d " " -f1`

function start {

    if [ "$lastlogged" == "LABEL" ]; then
        echo "ERROR! cannot start tracking new task until previous has been stopped." >&2
        exit
    fi

    # add task start to logfile
    echo "START `date`" >> $LOGFILE
    echo "LABEL $1" >> $LOGFILE
}

function stop {

    if [ "$lastlogged" != "LABEL" ]; then
        echo "no tasks are currently being tracked."
        exit
    fi
    
    # add task end to logfile
    echo "END `date`" >> $LOGFILE
    echo "Stopped tracking task."
}

function status {

    case "$lastlogged" in
        "LABEL") echo "currently tracking task \"`tail -n 1 $LOGFILE | cut -d " " -f2-`\"" ;;
        *) echo "no tasks are currently being tracked" ;;
    esac
}

function usage {
    echo "
valid arguments:
    start [label]
        should start new timer, unless one is already running (error)
    stop
        should stop running timer. (error if no task is running?)
    status 
        displays what task is currently being tracked."
}

function log {
    echo "Logging time spent on tasks"
    echo ""

    declare -i start_sec
    declare -i end_sec
    start_sec=0; end_sec=0; label_text="unknown"

    while read l; do
        # Logfile format: [label] [content] where content is date or tasktext
        label=`echo $l | cut -d " " -f1`
        content=`echo $l | cut -d " " -f2-`

        case "$label" in
            "START") 
                start_sec=`date -d "$content" +%s`;;
            "LABEL") 
                label_text="$content";;
            "END") 
                end_sec=`date -d "$content" +%s`

                secs=$((end_sec-start_sec))
                printf "spent %02d:%02d:%02d on \"$label_text\"\n" $(($secs/3600)) $(($secs%3600/60)) $(($secs%60))

                start_sec=0; end_sec=0; label_text="unknown";;
            *) 
                echo "error: logfile not following set format";;
        esac
    done <$LOGFILE

    echo ""
    status
}

if [ $# -gt 0 ]; then

    case "$1" in
        "start") 
            start $2 ;;
        "stop") 
            stop ;;
        "status") 
            status ;;
        "log")
            log ;;
        *)
            echo "unknown argument \"$1\""; usage; exit ;;
    esac
fi
