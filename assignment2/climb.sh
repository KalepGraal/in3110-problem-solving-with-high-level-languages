#!/bin/bash

# climbs n directories up, where n is an optional argument
function climb {
    path=".."
    if [ $# -eq 1 ]; then
        for i in `seq 2 $1`
        do
            path="$path/.."
        done
        path="$path/"
    fi
    cd $path
}
