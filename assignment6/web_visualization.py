from flask import Flask
from flask import render_template
from flask import request
import data.data as d
from fitting import Classifier
from visualize import visualize_from

app = Flask(__name__)


class Project:
    title = "Assignment6 - Chrisfku"
    valid_score, test_score = 0.0, 0.0
    graph = ""


project = Project()


@app.route("/")
def index():
    return render_template("landingpage.html", project=project)


@app.route("/help")
def help():
    return render_template("help.html", project=project)


@app.route("/show_graph", methods=["POST"])
def show_graph():
    assert request.method == 'POST'

    classifier_type = request.form["classifier"]
    feature1 = request.form["feature1"]
    feature2 = request.form["feature2"]

    if feature1 == feature2:
        return render_template("landingpage.html", project=project, error="Cannot choose the same feature twice!")

    print(f"testing, classifier={classifier_type}, feature1={feature1}, feature2={feature2}")

    data = d.load_dataset("diabetes.csv")

    Xtrain, Xtest, ytrain, ytest = d.create_subsets(data[[feature1, feature2, "diabetes"]])

    clf = Classifier(classifier_type)
    clf.fit(Xtrain, ytrain)
    project.valid_score, project.test_score = clf.get_accuracy(Xtrain, Xtest, ytrain, ytest)
    graph_loc = visualize_from(Xtrain, ytrain, clf, feature1, feature2)
    project.graph = graph_loc

    return render_template("result.html", project=project)


if __name__ == '__main__':
    app.run(port=5000, debug=True) # debug=True # to autoreload on code changes..