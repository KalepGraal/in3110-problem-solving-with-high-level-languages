import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


# TODO: requirements.txt, readme.md
# TODO: docstrings
def scatter_from(dataset, target, dim1, dim2):
    # plot where target is True
    plt.scatter(dataset[target][dim1],
                dataset[target][dim2],
                c="red", label="yes", alpha=0.5)

    # plot where target is False
    plt.scatter(dataset[~target][dim1],
                dataset[~target][dim2],
                c="green", label="no", alpha=0.5)

    plt.xlabel(dim1)
    plt.ylabel(dim2)

    plt.legend(title="Has diabetes")
    plt.show()


def load_dataset(path):
    samples = pd.read_csv(path)

    # remove any incomplete data samples
    samples = samples.dropna(how="any")

    # drop id column, as it is not needed for model
    samples = samples.drop("id", axis=1)

    # replace neg/pos with true/false
    samples["diabetes"] = samples["diabetes"].replace("neg", False)
    samples["diabetes"] = samples["diabetes"].replace("pos", True)

    return samples


def create_subsets(dataset):
    return train_test_split(dataset.drop("diabetes", axis=1),
                            dataset["diabetes"], test_size=0.2)


if __name__ == '__main__':

    data = load_dataset("../diabetes.csv")

    # split into training and validation sets
    Xtrain, Xtest, ytrain, ytest = create_subsets(data)

    scatter_from(Xtrain, ytrain, "glucose", "insulin")
    scatter_from(Xtest, ytest, "glucose", "insulin")
    scatter_from(pd.concat([Xtrain, Xtest]), pd.concat([ytrain, ytest]), "glucose", "insulin")
