import matplotlib.pyplot as plt
import matplotlib.colors as clr
import data.data as d
import numpy as np
import datetime
from sklearn.preprocessing import StandardScaler
from fitting import Classifier


def visualize_from(dataset, target, classifier, dim1, dim2):

    x_min = dataset[dim1].min() - 10
    x_max = dataset[dim1].max() + 10

    y_min = dataset[dim2].min() - 10
    y_max = dataset[dim2].max() + 10

    xx, yy = np.meshgrid(np.arange(x_min, x_max, 1),
                         np.arange(y_min, y_max, 1))

    X = np.c_[xx.ravel(), yy.ravel()]

    if classifier.uses_standard_scaler:
        X = StandardScaler().fit_transform(X)

    Z = classifier.predict(X)
    Z = Z.reshape(xx.shape)

    cmap = clr.ListedColormap(["green", "red"])
    plt.contourf(xx, yy, Z, cmap=cmap, alpha=0.25, antialiased=True)
    # plt.contour(xx, yy, Z, colors="k", linewidths=0.7, alpha=0.5)

    # plot where target is True
    plt.scatter(dataset[target][dim1],
                dataset[target][dim2],
                c="red", label="yes", alpha=0.5)

    # plot where target is False
    plt.scatter(dataset[~target][dim1],
                dataset[~target][dim2],
                c="green", label="no", alpha=0.5)

    plt.xlabel(dim1)
    plt.ylabel(dim2)
    plt.title(classifier.clf.__class__.__name__)

    plt.legend(title="Has diabetes")

    datestr = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")

    savelocation = f"static/images/plot_{datestr}.png"
    plt.savefig(savelocation)
    plt.show()

    return savelocation


if __name__ == '__main__':
    data = d.load_dataset("diabetes.csv")

    dim1 = "glucose"
    dim2 = "insulin"
    target = "diabetes"

    Xtrain, Xtest, ytrain, ytest = d.create_subsets(data[[dim1, dim2, target]])

    classifier = Classifier("LinearSVC")
    classifier.fit(Xtrain, ytrain)
    visualize_from(Xtrain, ytrain, classifier, dim1, dim2)
    # visualize_from(Xtest, ytest, classifier, dim1, dim2)

    classifier = Classifier("KNeighbors")
    classifier.fit(Xtrain, ytrain)
    visualize_from(Xtrain, ytrain, classifier, dim1, dim2)
    # visualize_from(Xtest, ytest, classifier, dim1, dim2)

    # classifier = Classifier(SVC(gamma="scale"))
    # classifier.fit(Xtrain, ytrain)
    # visualize_from(Xtrain, ytrain, classifier, dim1, dim2)
    # visualize_from(Xtest, ytest, classifier, dim1, dim2)

    #classifier = fit_linear_svc(Xtest, ytest)
    #visualize_from(Xtest, ytest, classifier, dim1, dim2)

