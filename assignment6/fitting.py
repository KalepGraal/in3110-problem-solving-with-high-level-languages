from sklearn import metrics
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

import data.data as d


class Classifier:
    def __init__(self, type):
        self.uses_standard_scaler = False

        if type == "LinearSVC":
            # TODO: Find good values for C?
            self.clf = LinearSVC(random_state=0, C=2, tol=1e-5, max_iter=5000)
            self.uses_standard_scaler = True
        elif type == "KNeighbors":
            self.clf = KNeighborsClassifier(n_neighbors=2)
        elif type == "SVC":
            self.clf = SVC(gamma="scale")

    def fit(self, X, y):
        # Apply StandardScaler if this is used
        if self.uses_standard_scaler:
            X = StandardScaler().fit_transform(X)

        # Create the model
        self.clf.fit(X, y)
        return self.clf

    def predict(self, X):
        return self.clf.predict(X)

    def get_accuracy(self, Xtrain, Xtest, ytrain, ytest):
        prediction = self.clf.predict(Xtrain)
        training_score = metrics.accuracy_score(ytrain, prediction)
        prediction = self.clf.predict(Xtest)
        validation_score = metrics.accuracy_score(ytest, prediction)
        return training_score, validation_score


def test_run(data, features):
    print(f"\nFeatures: {features}")
    Xtrain, Xtest, ytrain, ytest = d.create_subsets(data)

    for c in ["LinearSVC", "KNeighbors", "SVC"]:
        clf = Classifier(c)
        clf.fit(Xtrain, ytrain)
        t_acc, v_acc = clf.get_accuracy(Xtrain, Xtest, ytrain, ytest)
        print(c + " --- training set: {:.2f}, validation set {:.2f}".format(t_acc, v_acc))


if __name__ == '__main__':
    data = d.load_dataset("diabetes.csv")

    test_run(data, ["pregnant","glucose","pressure","triceps","insulin","mass","pedigree","age","diabetes"])
    test_run(data, ["pregnant", "insulin", "mass", "pedigree", "age", "diabetes"])
    test_run(data, ["pregnant", "pressure", "insulin", "mass", "pedigree", "diabetes"])
    test_run(data, ["pregnant", "insulin", "mass", "diabetes"])
    test_run(data, ["pregnant","glucose","pressure","triceps","insulin","mass","pedigree","age","diabetes"])

