#!/usr/bin/env python3
import unittest
from complex import Complex


class ComplexTest(unittest.TestCase):
    def test_add(self):
        """ Tests for adding two complex numbers """
        failmessage = "Adding two complex numbers produces unexpected result"
        self.assertEqual(Complex(1, 2) + Complex(0, 1), Complex(1, 3), failmessage)
        self.assertEqual(Complex(0, 0) + Complex(0, 0), Complex(0, 0), failmessage)
        self.assertEqual(Complex(-3, 5) + Complex(7, -4), Complex(4, 1), failmessage)

    def test_sub(self):
        """ Tests for subtracting from complex numbers """
        failmessage = "Subtracting produces unexpected result"
        self.assertEqual(Complex(1, 2) - Complex(0, 1), Complex(1, 1), failmessage)
        self.assertEqual(Complex(0, 0) - Complex(0, 0), Complex(0, 0), failmessage)
        self.assertEqual(Complex(-3, 5) - Complex(7, -4), Complex(-10, 9), failmessage)

    def test_mul(self):
        """ Tests for multiplication with complex numbers """
        self.assertEqual(Complex(5, 5) * Complex(5, 5), Complex(0, 50), "failed multiplication with complex")
        self.assertEqual(Complex(2, 7) * Complex(-3, 4), Complex(-34, -13), "failed multiplication with complex")
        self.assertEqual(Complex(5, 5) * 2, Complex(10, 10), "failed multiplication with constant")

    def test_conjugate(self):
        """ Tests for getting the conjugate from complex numbers """
        failmessage = "Conjugate produces unexpected result"
        self.assertEqual(Complex(1, 2).conjugate(), Complex(1, -2), failmessage)
        self.assertEqual(Complex(0, 0).conjugate(), Complex(0, 0), failmessage)
        self.assertEqual(Complex(-3, -7).conjugate(), Complex(-3, 7), failmessage)

    def test_modulus(self):
        """ Tests for getting the modulus from complex numbers """
        failmessage = "Modulus produces unexpected result"
        self.assertEqual(Complex(4, 3).modulus(), 5, failmessage)
        self.assertEqual(Complex(0, 0).modulus(), 0, failmessage)
        self.assertEqual(Complex(-3, -4).modulus(), 5, failmessage)
        self.assertEqual(Complex(10, 0).modulus(), 10, failmessage)
        self.assertEqual(Complex(0, 10).modulus(), 10, failmessage)

    def test_equals(self):
        """ Tests for Complex's equality function """
        self.assertEqual(Complex(1, 2), Complex(1, 2), "Complex numbers should be equal to themselves")
        self.assertNotEqual(Complex(1, 2), Complex(2, 1), "Different complex numbers should not be equal")
        self.assertEqual(Complex(0, 0), Complex(0, 0), "0 should be equal to 0")

    def test_rmul(self):
        """ Tests for interop with pythons complex numbers and constants

        Tests that our own Complex class can be used with pythons complex numbers,
        and that these can be used together with constants, tests for addition, multiplication and subtraction."""
        self.assertEqual(2 * Complex(5, 5), Complex(10, 10), "failed multiplication with constant")
        self.assertEqual(Complex(5, 5) * (5+5j), Complex(0, 50), "failed multiplication with pythons complex")
        self.assertEqual((2+7j) * Complex(-3, 4), Complex(-34, -13), "failed multiplication with pythons complex")
        self.assertEqual(Complex(5, 5) * 2, Complex(10, 10), "failed multiplication with pythons constant")

        self.assertEqual(Complex(2, 3) + (2+2j), Complex(4, 5))
        self.assertEqual(4 * Complex(3, 4) - 2, Complex(10, 16))


if __name__ == '__main__':
    unittest.main()


