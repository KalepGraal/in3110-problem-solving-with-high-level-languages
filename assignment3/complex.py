#!/usr/bin/env python3
import math


class Complex:
    """ Container class to represent complex numbers """

    def __init__(self, real, imaginary):
        """ Constructor for Complex class

        :param real: numeric value representing the real part of the complex number
        :param imaginary: numeric value representing the imaginary part of the complex number
        :returns new Complex object
        """
        self.real = real
        self.imaginary = imaginary

    def __str__(self):
        """ Converts a Complex number to a string representation.

        :returns String representation of complex number.
        """
        string = f"{self.real}"
        if self.imaginary >= 0:
            string += "+"
        return string + f"{self.imaginary}i"

    def conjugate(self):
        """ Gets the complex numbers conjugate.

        :returns conjugate of complex number.
        """
        return Complex(self.real, -self.imaginary)

    def modulus(self):
        """ Gets the complex numbers modulus.

        :returns modulus of complex number.
        """
        return math.sqrt(self.real ** 2 + self.imaginary ** 2)

    def __add__(self, other):
        """ Add operation for complex numbers

        :returns Complex number result of addition.
        """
        if isinstance(other, int):
            return Complex(self.real + other, self.imaginary)
        if isinstance(other, complex):
            return self + Complex(other.real, other.imag)

        r = self.real + other.real
        i = self.imaginary + other.imaginary
        return Complex(r, i)

    def __sub__(self, other):
        """ Subtraction operation for complex numbers

        :returns Complex number result of subtraction.
        """
        if isinstance(other, int):
            return Complex(self.real - other, self.imaginary)
        if isinstance(other, complex):
            return self - Complex(other.real, other.imag)

        r = self.real - other.real
        i = self.imaginary - other.imaginary
        return Complex(r, i)

    def __mul__(self, other):
        """ Multiplication operation for complex numbers

        :returns Complex number result of multiplication.
        """
        if isinstance(other, int):
            return Complex(self.real * other, self.imaginary * other)
        if isinstance(other, complex):
            return self * Complex(other.real, other.imag)

        r = self.real * other.real - self.imaginary * other.imaginary
        i = self.real * other.imaginary + other.real * self.imaginary
        return Complex(r, i)

    def __eq__(self, other):
        """ Equality function for complex numbers.

        :returns (bool) Returns true if compared complex numbers are equal, else false
        """
        return self.real == other.real and self.imaginary == other.imaginary

    # Assignment 3.4
    def __radd__(self, other):
        """ Function to make addition with complex numbers be supported even if left operand does not.

        :returns Complex number result of addition
        """
        return self + other

    def __rsub__(self, other):
        """ Function to make subtraction with complex numbers be supported even if left operand does not.

        :returns Complex number result of subtraction
        """
        return self - other

    def __rmul__(self, other):
        """ Function to make multiplication with complex numbers be supported even if left operand does not.

        :returns Complex number result of multiplication
        """
        return self * other

    def __complex__(self):
        """ Converts a Complex number into pythons own complex number representation

        :returns Complex number converted to pythons own complex number representation
        """
        return complex(self.real, self.imaginary)
