# Assignment 3 - Chrisfku

## 3.1 wc
Here my implementation is based on the assignment text, and not on how *wc* is implemented.  
Where *wc* only counts newlines, my implementation counts the number of lines.  
*wc* also does not display the number of characters, but rather the bytecount.

To use wc from anywhere, add the following to your .bashrc file:  
``alias wc="Python3 [Path to wc.py]"``

Usage: ``wc [FILE]...`` (if alias is set)  
or if alias is not set: ``Python3 wc.py [FILE]...``

## 3.2 Unit tests
Implemented using pythons unittests, run with:  ``python3 test_complex.py``

## 3.3-3.4 Complex numbers
Created class Complex to represent complex numbers. Should support interop with pythons own complex numbers.
