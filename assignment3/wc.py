#!/usr/bin/env python3
import sys
import re


def print_counts(filename):
    """ Prints word, line and char count from file.

    :param filename: name of file from which to print word, line and char count.
    :returns None
    """
    try:
        f = open(filename)

        filetext = f.read()
        lines = filetext.count("\n") + 1
        words = len(re.findall("\S+", filetext))
        chars = len(re.findall(".", filetext))

        print(f"{lines} {words} {chars} {filename}")

        f.close()
    except IOError:
        print(f"did not find file: {filename}")
        exit(1)


if __name__ == '__main__':
    """ Prints count for each argument filename passed """
    for i in range(1, len(sys.argv)):
        print_counts(sys.argv[i])